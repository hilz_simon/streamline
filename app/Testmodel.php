<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testmodel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'testmodels';
}
