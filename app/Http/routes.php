<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Testmodel;

Route::get('/', function () {

    $flight = new Testmodel();

    $flight->name = 'simon ' . uniqid();

    $flight->save();

    return view('welcome', ['testmodels' => Testmodel::all()]);
});
