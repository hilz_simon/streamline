FROM mikechernev/php7-fpm-laravel

# setup composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('SHA384', 'composer-setup.php') === '92102166af5abdb03f49ce52a40591073a7b859a86e8ff13338cf7db58a19f7844fbc0bb79b2773bf30791e935dbd938') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer

# composer dependencies
RUN apt-get install -y git unzip

# add app and php.ini to image
ADD . /var/www/html
ADD ./.streamline/conf/php/php.ini /usr/local/etc/php/php.ini

# specify app as volume to be able to mount it in nginx
VOLUME /var/www/html

# composer install
RUN cd /var/www/html && composer install

EXPOSE 9000

CMD [ "php-fpm" ]